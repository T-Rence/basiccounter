﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ClasCompteur;

namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestConstructor()
        {
            Compteur cpt_test = new Compteur();
            Assert.AreEqual(0, cpt_test.nb);
        }
        [TestMethod]
        public void TestIncrementation()
        {
            Compteur cpt_test = new Compteur();
            Assert.AreEqual("1", cpt_test.incrementation());
        }
        [TestMethod]
        public void TestDecrementation()
        {
            Compteur cpt_test = new Compteur();
            Assert.AreEqual("-1", cpt_test.decrementation());
        }
        
    }
}
