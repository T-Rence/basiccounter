﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using ClasCompteur;
using System.Threading.Tasks;
using System.Windows.Forms;




namespace BasicCounter
{
    
    public partial class Form1 : Form
    {
        Compteur cpt;
        public Form1()
        {
            InitializeComponent();
            this.cpt = new Compteur();
        }

        private void moins_Click(object sender, EventArgs e)
        {
            label1.Text = this.cpt.decrementation();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            label1.Text = this.cpt.incrementation();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            label1.Text = this.cpt.raz();
        }
    }
}
